package jp.hellodea.sample.common.util;

import com.amazonaws.mobileconnectors.s3.transfermanager.Transfer;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.mobileconnectors.s3.transfermanager.Upload;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.File;
import java.io.InputStream;

import de.greenrobot.event.EventBus;
import jp.hellodea.sample.common.AWSCredential;
import jp.hellodea.sample.common.eventbus.ProgressEvent;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/02/17.
 */
public class S3Util {

    /**
     * upload stream to s3
     * @param bucket
     * @param fileName
     * @param mimeType
     * @param size
     * @param inputStream
     * @param progressEventKey
     * @param progressOffset
     * @param maxProgress
     */
    public static Transfer.TransferState uploadStream(String bucket, String fileName, String mimeType, long size,
                                    InputStream inputStream, Object progressEventKey,
                                    int progressOffset, int maxProgress){

        //set upload metadata
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(size);
        objectMetadata.setContentType(mimeType);

        TransferManager transferManager = new TransferManager(AWSCredential.getCredentials());

        //upload to s3
        Upload upload = transferManager.upload(bucket,fileName,inputStream,objectMetadata);

        long total = upload.getProgress().getTotalBytesToTransfer();

        float progressWidth = maxProgress - progressOffset;

        //check update progress
        while (!upload.isDone()) {
            long transferred = upload.getProgress().getBytesTransferred();
            int progress = progressOffset
                    + (int) (((float)transferred / (float)total) * progressWidth);

            //send progress
            EventBus.getDefault().post(new ProgressEvent(progressEventKey,100,progress));

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                Timber.e(e, e.getMessage());
                //can not handle this exception
                throw new RuntimeException(e);
            }
        }

        return upload.getState();

    }

    /**
     * upload file to S3
     *
     * @param bucket
     * @param fileName
     * @param file
     * @param progressEventKey
     * @param progressOffset
     * @param maxProgress
     * @return
     */
    public static Transfer.TransferState uploadFile(String bucket, String fileName, String mimeType,
                  File file, Object progressEventKey, int progressOffset, int maxProgress){

        TransferManager transferManager = new TransferManager(AWSCredential.getCredentials());

        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(mimeType);

        PutObjectRequest objectRequest =
                new PutObjectRequest(bucket,fileName,file).withMetadata(objectMetadata);

        //upload to s3
        Upload upload = transferManager.upload(objectRequest);

        long total = upload.getProgress().getTotalBytesToTransfer();

        float progressWidth = maxProgress - progressOffset;

        //check update progress
        while (!upload.isDone()) {
            long transferred = upload.getProgress().getBytesTransferred();
            int progress = progressOffset
                    + (int) (((float)transferred / (float)total) * progressWidth);

            //send progress
            EventBus.getDefault().post(new ProgressEvent(progressEventKey,100,progress));

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                Timber.e(e, e.getMessage());
                //can not handle this exception
                throw new RuntimeException(e);
            }
        }

        return upload.getState();

    }

}
