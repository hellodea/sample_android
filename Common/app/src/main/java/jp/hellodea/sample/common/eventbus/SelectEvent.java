package jp.hellodea.sample.common.eventbus;

/**
 * selection event like select item on list and so on.
 * @author kazuhiro
 *
 */
public class SelectEvent extends BusEvent {

	public SelectEvent(Object eventKey, Object arg) {
		super(eventKey, arg);
	}

    public SelectEvent(Object eventKey, Object... args) {
        super(eventKey, args);
    }

    public SelectEvent() {
    }

    public SelectEvent(Object eventKey) {
        super(eventKey);
    }
}
