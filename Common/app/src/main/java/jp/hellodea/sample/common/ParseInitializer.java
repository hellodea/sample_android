package jp.hellodea.sample.common;

import android.app.Application;
import android.content.Context;

import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseUser;

import jp.hellodea.sample.common.entity.Comment;
import jp.hellodea.sample.common.entity.Diary;
import jp.hellodea.sample.common.entity.DiaryUser;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class ParseInitializer {
    public static void initialize(Context context){

        ParseObject.registerSubclass(DiaryUser.class);
        ParseObject.registerSubclass(Diary.class);
        ParseObject.registerSubclass(Comment.class);
        Parse.enableLocalDatastore(context);
        Parse.initialize(context, ParseConst.APPLICATION_ID, ParseConst.CLIENT_ID);

        //activate anonymous user
        ParseUser.enableAutomaticUser();
    }

}
