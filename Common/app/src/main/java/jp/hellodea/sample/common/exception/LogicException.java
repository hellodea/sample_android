package jp.hellodea.sample.common.exception;

/**
 * exception for logic
 * @author kazuhiro
 *
 */
public class LogicException extends Exception {

    private static final long serialVersionUID = 1L;

    private ErrorCode errorCode;
	private int reasonRes;
	
	public ErrorCode getErrorCode() {
		return errorCode;
	}
    public int getReasonRes() {
        return reasonRes;
    }

	public LogicException(ErrorCode pErrorCode, Throwable throwable) {
		super(throwable);
		errorCode = pErrorCode;
	}

	public LogicException(ErrorCode pErrorCode,int pReasonRes, Throwable throwable) {
		super(throwable);
		errorCode = pErrorCode;
		reasonRes = pReasonRes;
	}

	public LogicException(ErrorCode pErrorCode,int pReasonRes) {
		errorCode = pErrorCode;
		reasonRes = pReasonRes;
	}


}
