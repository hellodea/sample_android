package jp.hellodea.sample.common.entity;

import com.parse.ParseACL;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.Date;

import hugo.weaving.DebugLog;
import jp.hellodea.sample.common.R;
import jp.hellodea.sample.common.exception.ErrorCode;
import jp.hellodea.sample.common.exception.LogicException;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/02/10.
 */
@ParseClassName("DiaryUser")
public class DiaryUser extends ParseObject{
    public static final String F_NICKNAME = "NICKNAME";

    /**
     * get current user
     * @return
     * @throws ParseException
     */
    @DebugLog
    public static DiaryUser getCurrent() throws LogicException {
        ParseUser parseUser = ParseUser.getCurrentUser();
        DiaryUser diaryUser;

        if(parseUser.getObjectId() == null){
            try {

                diaryUser = new DiaryUser();
                diaryUser.put(DiaryUser.F_NICKNAME, "no name");
                diaryUser.save();
                diaryUser.pin();

                //create new user
                ParseACL acl = new ParseACL();
                acl.setPublicReadAccess(false);
                acl.setPublicWriteAccess(false);
                parseUser.setACL(acl);
                parseUser.setUsername(String.valueOf(new Date().getTime()));
                parseUser.setPassword(String.valueOf(new Date().getTime()));
                parseUser.put(DiaryUser.class.getSimpleName(), diaryUser);
                parseUser.signUp();

                //default acl
                acl = new ParseACL();
                acl.setPublicWriteAccess(false);
                acl.setPublicReadAccess(true);
                ParseACL.setDefaultACL(acl,true);

            } catch (ParseException e) {
                Timber.e(e, "ParseException %s", e.getMessage());
                throw new LogicException(ErrorCode.CanNotSave,R.string.common_error_user_save,e);
            }
        }else{
            diaryUser = (DiaryUser) parseUser.getParseObject(DiaryUser.class.getSimpleName());
        }

        return diaryUser;
    }
}
