package jp.hellodea.sample.common.eventbus;

/**
 * progress event
 * @author kazuhiro
 *
 */
public class ProgressEvent extends BusEvent {
    public int max;
    public int progress;

	public ProgressEvent(Object eventKey, int pMax, int pProgress) {
		super(eventKey);

        max = pMax;
        progress = pProgress;
	}
}
