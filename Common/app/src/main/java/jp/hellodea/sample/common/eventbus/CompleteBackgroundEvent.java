package jp.hellodea.sample.common.eventbus;

/**
 * something completed event in background process
 * @author kazuhiro
 *
 */
public class CompleteBackgroundEvent extends BusEvent {

	public CompleteBackgroundEvent(Object eventKey, Object arg) {
		super(eventKey, arg);
	}

    public CompleteBackgroundEvent(Object eventKey, Object... args) {
        super(eventKey, args);
    }

    public CompleteBackgroundEvent() {
    }

    public CompleteBackgroundEvent(Object eventKey) {
        super(eventKey);
    }
}
