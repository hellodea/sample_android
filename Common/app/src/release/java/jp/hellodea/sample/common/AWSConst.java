package jp.hellodea.sample.common;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class AWSConst {
    public static final String ACCESS_KEY = "";
    public static final String SECRET_ACCESS_KEY = "/";

    public static final String BUCKET = "hellodea-sample-app";
    public static final String KEY_PREFIX_PICTURE = "/pictures/";
    public static final String KEY_POSTFIX_THUMBNAIL_PICTURE = "_thumb";
    public static final String CLOUD_FRONT = "https://d36nnxiv9tr69f.cloudfront.net/";

}
