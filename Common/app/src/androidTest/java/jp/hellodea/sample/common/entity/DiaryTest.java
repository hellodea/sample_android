package jp.hellodea.sample.common.entity;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.UUID;

import jp.hellodea.sample.common.ParseInitializer;
import jp.hellodea.sample.common.exception.LogicException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by kazuhiro on 2015/03/17.
 */
@RunWith(AndroidJUnit4.class)
public class DiaryTest {

    @BeforeClass
    public static void initWholeTest(){
        Context context = InstrumentationRegistry.getTargetContext();
        ParseInitializer.initialize(context);

        return;

    }

    @Before
    public void initEachTest() throws ParseException {
        ParseObject.unpinAll();
    }

    @After
    public void finishEachTest(){

    }


    @Test
    public void test() throws ParseException, LogicException {

    }

}
