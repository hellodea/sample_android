package jp.hellodea.sample.diary.testapplication;

import android.app.Application;
import android.util.Log;

import jp.hellodea.sample.common.*;
import jp.hellodea.sample.common.objectgraph.CommonModule;
import jp.hellodea.sample.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.sample.diary.DiaryModule;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class DiaryApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("DiaryApp.onCreate", jp.hellodea.sample.common.BuildConfig.BUILD_TYPE);

        TimberInitializer.initialize();

        ParseInitializer.initialize(this);

        ObjectGraphHolder.plus(new CommonModule(this));
        ObjectGraphHolder.plus(new DiaryModule());

        JobManagerHolder.initialize(this);

    }

}
