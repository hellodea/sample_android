package jp.hellodea.sample.diary.logic;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import jp.hellodea.sample.common.ParseInitializer;
import jp.hellodea.sample.common.entity.Diary;
import jp.hellodea.sample.common.entity.DiaryUser;
import jp.hellodea.sample.common.exception.LogicException;

import static org.junit.Assert.*;

/**
 * Created by kazuhiro on 2015/03/17.
 */
@RunWith(AndroidJUnit4.class)
public class DiaryLogicTest {

    DiaryLogic diaryLogic;

    @BeforeClass
    public static void initWholeTest(){
        Context context = InstrumentationRegistry.getTargetContext();
        ParseInitializer.initialize(context);

        return;

    }

    @Before
    public void initEachTest() throws ParseException {
        ParseObject.unpinAll();
        diaryLogic = new DiaryLogic();
    }

    @After
    public void finishEachTest(){

    }


    @Test
    public void testGetDiaries() throws ParseException, LogicException {

        int count = ParseQuery.getQuery(Diary.class).fromLocalDatastore().count();

        //Check the prerequisites
        assertEquals(0,count);

        //test data
        for(int i = 0; i < 10; i++){
            Diary diary = new Diary();
            diary.put(Diary.F_AUTHOR, DiaryUser.getCurrent());
            diary.put(Diary.F_TITLE,"test" + i);
            diary.put(Diary.F_BODY, "test body");
            diary.pin();
        }

        //perform test target
        List<Diary> diaries = diaryLogic.getDiaries();

        //check the results
        assertEquals(10,diaries.size());
    }

    @Test
    public void testGetDiary() throws ParseException, LogicException {
        int count = ParseQuery.getQuery(Diary.class).fromLocalDatastore().count();

        //Check the prerequisites
        assertEquals(0,count);

        //test data
        String proxyId = UUID.randomUUID().toString();
        Diary diary = new Diary();
        diary.put(Diary.F_PROXY_ID, proxyId);
        diary.put(Diary.F_AUTHOR, DiaryUser.getCurrent());
        diary.put(Diary.F_TITLE,"test");
        diary.put(Diary.F_BODY, "test body");
        diary.pin();

        //perform test target
        diary = diaryLogic.getDiary(proxyId);

        //check the results
        assertNotNull(diary);
        assertEquals("test",diary.getString(Diary.F_TITLE));


    }

}
