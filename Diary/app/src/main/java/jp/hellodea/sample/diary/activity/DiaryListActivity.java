package jp.hellodea.sample.diary.activity;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import org.androidannotations.annotations.EActivity;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.sample.common.eventbus.CallScreenEvent;
import jp.hellodea.sample.diary.R;
import jp.hellodea.sample.diary.fragment.DiaryFragment;
import jp.hellodea.sample.diary.fragment.DiaryListFragment_;
import jp.hellodea.sample.diary.fragment.EditFragment;

@SuppressLint("Registered")
@EActivity(resName = "activity_diary_list")
public class DiaryListActivity extends ActionBarActivity {

    @DebugLog
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            Fragment fragment = DiaryListFragment_.builder().build();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }
    }

    @DebugLog
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @DebugLog
    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @DebugLog
    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);
    }

    @DebugLog
    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);

    }

    @DebugLog
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * change new fragment screen
     * @param event
     */
    @DebugLog
    public void onEventMainThread(CallScreenEvent event){
        if(event.getEventKey().equals(EditFragment.class)){
            //Show editor screen
            EditActivity_.intent(this).start();
        }else if(event.getEventKey().equals(DiaryFragment.class)){
            //Show detail screen
            String id = (String) event.getArg();
            DiaryActivity_.intent(this).diaryProxyId(id).start();
        }
    }

}
