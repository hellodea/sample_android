package jp.hellodea.sample.diary.fragment;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.sample.common.JobManagerHolder;
import jp.hellodea.sample.common.entity.Diary;
import jp.hellodea.sample.common.entity.DiaryUser;
import jp.hellodea.sample.common.eventbus.CallScreenEvent;
import jp.hellodea.sample.common.eventbus.CompleteBackgroundEvent;
import jp.hellodea.sample.common.eventbus.ErrorEvent;
import jp.hellodea.sample.common.eventbus.ScreenResultEvent;
import jp.hellodea.sample.common.exception.LogicException;
import jp.hellodea.sample.common.fragment.AlertDialogFragment;
import jp.hellodea.sample.common.fragment.AlertDialogFragment_;
import jp.hellodea.sample.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.sample.diary.DiaryModule;
import jp.hellodea.sample.diary.R;
import jp.hellodea.sample.diary.logic.DiaryLogic;
import jp.hellodea.sample.diary.logic.PostDiaryJob;

/**
 * Edit diary and post to the server.
 */
@EFragment(resName = "fragment_edit")
@OptionsMenu(resName = "menu_edit")
public class EditFragment extends Fragment implements Validator.ValidationListener{

    public static final int REQUEST_CODE_SELECT_PICTURE = 1;

    Validator validator;

    @Inject
    DiaryLogic diaryLogic;

    @FragmentArg
    String diaryProxyId;

    @ViewById
    @NotEmpty
    EditText editTextTitle;

    @ViewById
    @NotEmpty
    EditText editTextBody;

    @ViewById
    TextView textViewPostTime;

    @ViewById
    ImageView imageViewPicture;

    @Click
    void buttonSelectPhoto(){
        actionShowPictureSelector();
    }

    Diary diary;

    @InstanceState
    Uri imageUri;

    @DebugLog
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @DebugLog
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @DebugLog
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @DebugLog
    @AfterViews
    public void afterViews(){}

    @DebugLog
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //initialize fragment
        ObjectGraphHolder.get().inject(this);
        setHasOptionsMenu(true);

        //prepare validator
        validator = new Validator(this);
        validator.setValidationListener(this);

        //prepare diary data
        getDiary(savedInstanceState);

    }

    @DebugLog
    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @DebugLog
    @Override
    public void onStart() {
        super.onStart();

        EventBus.getDefault().registerSticky(this);
    }

    @DebugLog
    @Override
    public void onResume() {
        super.onResume();

    }

    @DebugLog
    @Override
    public void onPause() {
        super.onPause();
    }

    @DebugLog
    @Override
    public void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);

    }

    @DebugLog
    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @DebugLog
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @DebugLog
    @Override
    public void onDetach() {
        super.onDetach();
    }

    @DebugLog
    @Background
    void getDiary(Bundle savedInstanceState){
        try{
            if(diaryProxyId != null) {
                diary = diaryLogic.getDiary(diaryProxyId);
            }else{
                diary = new Diary();

                diary.put(Diary.F_AUTHOR, DiaryUser.getCurrent());

                //use for find instead of objectId
                diary.put(Diary.F_PROXY_ID,UUID.randomUUID().toString());
                diary.put(Diary.F_POST_TIME, new Date());
            }

        } catch (LogicException e) {
            EventBus.getDefault().postSticky(new ErrorEvent(this.getClass(), e));
            return;
        }

        //complete getting diary
        EventBus.getDefault().postSticky(new CompleteBackgroundEvent(this.getClass(),savedInstanceState));
    }

    /**
     * show diary
     */
    @DebugLog
    public void onEventMainThread(CompleteBackgroundEvent event) {
        if (!event.getEventKey().equals(this.getClass())) return;

        //set data at first time
        if(event.getArg() == null){
            editTextTitle.setText(diary.getString(Diary.F_TITLE));
            editTextBody.setText(diary.getString(Diary.F_BODY));
        }

        CharSequence timeString = DateUtils.getRelativeTimeSpanString(
                diary.getDate(Diary.F_POST_TIME).getTime());
        textViewPostTime.setText(timeString);

        //show the picture when a picture is already selected
        if(imageUri != null){
            Glide.with(this).load(imageUri).into(imageViewPicture);
        } else if(diary.getObjectId() != null){
            Glide.with(this).load(diary.getImageUrl(false)).into(imageViewPicture);
        }

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }

    @DebugLog
    @OptionsItem
    void actionSave(){
        validator.validate();
    }

    /**
     * show picture selector screen
     */
    void actionShowPictureSelector(){
        EventBus.getDefault().post(new CallScreenEvent(Intent.ACTION_PICK, REQUEST_CODE_SELECT_PICTURE));
    }

    /**
     * get selected picture
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ScreenResultEvent event){
        if((Integer)event.getEventKey() != REQUEST_CODE_SELECT_PICTURE) return;

        //get a picture from this intent
        Intent intent = (Intent) event.getArg();
        imageUri = intent.getData();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            getActivity().getContentResolver().takePersistableUriPermission(imageUri,Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }

        Glide.with(this).load(imageUri).into(imageViewPicture);


        //remove the sticky event after using
        EventBus.getDefault().removeStickyEvent(event);
    }

    /**
     * Post the diary data to the server.
     */
    @Override
    @Background
    public void onValidationSucceeded() {

        //set input data to the diary entity
        diary.put(Diary.F_TITLE,editTextTitle.getText().toString());
        diary.put(Diary.F_BODY,editTextBody.getText().toString());

        //set picture id if image was selected
        if(imageUri != null)
            diary.put(Diary.F_PICTURE_ID, UUID.randomUUID().toString());

        try {
            //save diary
            diaryLogic.saveDiary(diary);

        } catch (LogicException e) {
            EventBus.getDefault().postSticky(new ErrorEvent(this.getClass(), e));
            return;
        }

        //schedule job to send data to server
        JobManagerHolder.get().addJob(new PostDiaryJob(diary.getString(Diary.F_PROXY_ID),
                imageUri != null ? imageUri.toString() : null));

        //show diary list screen after saving
        EventBus.getDefault().postSticky(new CallScreenEvent(DiaryListFragment.class));
    }

    /**
     * show validation errors
     * @param errors
     */
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for(ValidationError error : errors){
            if(error.getView().equals(editTextTitle)){
                editTextTitle.setError(getResources().getString(R.string.diary_validation_title));
                continue;
            }
            if(error.getView().equals(editTextBody)){
                editTextBody.setError(getResources().getString(R.string.diary_validation_body));
            }
        }
    }

    /**
     * error handling
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ErrorEvent event){

        if(!event.getEventKey().equals(this.getClass())) return;

        LogicException e = (LogicException) event.getException();

        //show dialog when error was happened
        DialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(AlertDialogFragment.class.getName())
                .titleRes(e.getErrorCode().getErrorRes())
                .messageRes(e.getReasonRes())
                .positiveRes(android.R.string.ok).build();
        dialogFragment.show(getActivity().getFragmentManager()
                ,AlertDialogFragment.class.getName());

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }

}
