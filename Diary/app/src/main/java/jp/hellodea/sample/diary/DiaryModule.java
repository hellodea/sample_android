package jp.hellodea.sample.diary;

import javax.inject.Singleton;

import dagger.Module;
import dagger.ObjectGraph;
import dagger.Provides;
import jp.hellodea.sample.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.sample.diary.fragment.DiaryFragment_;
import jp.hellodea.sample.diary.fragment.DiaryListFragment_;
import jp.hellodea.sample.diary.fragment.EditFragment_;
import jp.hellodea.sample.diary.logic.DiaryLogic;
import jp.hellodea.sample.diary.logic.PostDiaryJob;

/**
 * Created by kazuhiro on 2014/12/18.
 */
@Module(
        injects = {
                EditFragment_.class,
                DiaryListFragment_.class,
                DiaryFragment_.class,
                PostDiaryJob.class
        },
        complete = false
)
public class DiaryModule {
    @Provides
    @Singleton
    public DiaryLogic provideDiaryLogic(){
        DiaryLogic diaryLogic = new DiaryLogic();
        return diaryLogic;
    }

}
