package jp.hellodea.sample.diary.logic;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.sample.common.eventbus.ProgressEvent;
import jp.hellodea.sample.common.exception.LogicException;
import jp.hellodea.sample.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.sample.diary.R;
import jp.hellodea.sample.diary.activity.DiaryActivity_;
import timber.log.Timber;

/**
 * post diary data to server and upload photo to s3
 * Created by kazuhiro on 2015/02/15.
 */
public class PostDiaryJob extends Job {

    public static final int NOTICE_ID = 1;

    @Inject
    transient Application application;

    @Inject
    transient DiaryLogic diaryLogic;

    private String diaryProxyId;
    private String imageUriStr;

    transient Notification.Builder builder;
    transient NotificationManager notificationManager;

    @DebugLog
    public PostDiaryJob(String pDiaryProxyId, String pImageUriStr) {
        super(new Params(1).requireNetwork().persist());

        diaryProxyId = pDiaryProxyId;
        imageUriStr = pImageUriStr;
    }

    @DebugLog
    @Override
    public void onAdded() {
    }

    @DebugLog
    @Override
    public void onRun() throws Throwable {
        ObjectGraphHolder.get().inject(this);


        Intent intent = DiaryActivity_.intent(application).diaryProxyId(diaryProxyId).get();
        PendingIntent pendingIntent =
                PendingIntent.getActivity(application,0,intent,PendingIntent.FLAG_CANCEL_CURRENT);

        builder = new Notification.Builder(application)
                .setContentTitle(application.getString(R.string.diary_notice_post_diary_title))
                .setContentText(application.getString(R.string.diary_notice_post_diary_message))
                .setSmallIcon(R.drawable.ic_cloud_upload_white_24dp)
                .setContentIntent(pendingIntent);

        notificationManager =
                (NotificationManager) application.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(NOTICE_ID,builder.build());

        EventBus.getDefault().register(this);

        try {
            //post diary
            diaryLogic.postDiary(application,diaryProxyId,imageUriStr);
        }finally {
            notificationManager.cancel(NOTICE_ID);

            EventBus.getDefault().unregister(this);
        }
    }

    @DebugLog
    @Override
    protected void onCancel() {

        Intent intent = DiaryActivity_.intent(application).diaryProxyId(diaryProxyId).get();
        PendingIntent pendingIntent =
                PendingIntent.getActivity(application,0,intent,PendingIntent.FLAG_CANCEL_CURRENT);

        builder = new Notification.Builder(application)
                .setContentTitle(application.getString(R.string.diary_notice_post_diary_error_title))
                .setContentText(application.getString(R.string.diary_notice_post_diary_error_message))
                .setSmallIcon(R.drawable.ic_cloud_upload_white_24dp)
                .setContentIntent(pendingIntent);
        notificationManager.notify(NOTICE_ID,builder.build());

    }

    @DebugLog
    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        Timber.e(throwable,"error %s",throwable.getMessage());

        //if LogicException was occurred, re-run this job
        return throwable instanceof LogicException;

    }

    @DebugLog
    public void onEvent(ProgressEvent event){
        if(event.getEventKey() != DiaryLogic.class) return;

        builder.setProgress(event.max,event.progress,false);
        notificationManager.notify(NOTICE_ID,builder.build());
    }


}
