package jp.hellodea.sample.diary.fragment;

import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ListView;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.sample.common.ParseFetch;
import jp.hellodea.sample.common.entity.Diary;
import jp.hellodea.sample.common.eventbus.CallScreenEvent;
import jp.hellodea.sample.common.eventbus.CompleteBackgroundEvent;
import jp.hellodea.sample.common.eventbus.ErrorEvent;
import jp.hellodea.sample.common.exception.LogicException;
import jp.hellodea.sample.common.fragment.AlertDialogFragment;
import jp.hellodea.sample.common.fragment.AlertDialogFragment_;
import jp.hellodea.sample.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.sample.common.view.EndlessScrollListener;
import jp.hellodea.sample.diary.R;
import jp.hellodea.sample.diary.adapter.DiaryAdapter;
import jp.hellodea.sample.diary.logic.DiaryLogic;
import timber.log.Timber;

/**
 */
@EFragment(resName = "fragment_diary_list")
@OptionsMenu(resName = "menu_diary_list")
public class DiaryListFragment extends Fragment {

    @Inject
    DiaryLogic diaryLogic;

    @ViewById
    ListView listViewDiary;

    @DebugLog
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //initialize fragment
        setHasOptionsMenu(true);
        ObjectGraphHolder.get().inject(this);

        listViewDiary.setOnScrollListener(new EndlessScrollListener() {
            @DebugLog
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                getDiaryList(ParseFetch.FROM_CLOUD, page - 1);
            }
        });

        //get data
        getDiaryList(ParseFetch.FROM_LOCAL, 0);

    }

    @DebugLog
    @Override
    public void onStart() {
        super.onStart();

        EventBus.getDefault().registerSticky(this);

    }

    @DebugLog
    @Override
    public void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);
    }

    /**
     * show diary detail
     * @param diary
     */
    @DebugLog
    @ItemClick
    public void listViewDiaryItemClicked(Diary diary){
        Timber.d("%s", diary.toString());
        EventBus.getDefault().post(new CallScreenEvent(DiaryFragment.class,diary.get(Diary.F_PROXY_ID)));
    }

    /**
     * open editor fragment
     */
    @DebugLog
    @OptionsItem
    void actionNew(){
        EventBus.getDefault().post(new CallScreenEvent(EditFragment.class));
    }

    /**
     * get diaries in background
     */
    @DebugLog
    @Background
    void getDiaryList(ParseFetch fetch, int page){
        List<Diary> diaries;

        try {
            diaries = diaryLogic.getDiaries(fetch, page,10);
        } catch (LogicException e) {
            EventBus.getDefault().postSticky(new ErrorEvent(this.getClass(), e));
            return;
        }

        //complete getting diary
        EventBus.getDefault().postSticky(new CompleteBackgroundEvent(this.getClass(), diaries, page));

        //refresh diaries
        if(fetch == ParseFetch.FROM_LOCAL){
            getDiaryList(ParseFetch.FROM_CLOUD, page);
        }
    }

    /**
     * show diaries
     */
    @DebugLog
    public void onEventMainThread(CompleteBackgroundEvent event){
        if(!event.getEventKey().equals(this.getClass())) return;

        Object[] args = event.getArgs();

        List<Diary> diaries = (List<Diary>) args[0];
        int page = (int) args[1];

        DiaryAdapter diaryAdapter;
        if(page == 0){
            diaryAdapter = new DiaryAdapter(getActivity(),R.layout.item_diary,diaries);
            listViewDiary.setAdapter(diaryAdapter);
        }else{
            diaryAdapter = (DiaryAdapter) listViewDiary.getAdapter();
            diaryAdapter.addAll(diaries);
        }

        diaryAdapter.notifyDataSetChanged();

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }

    /**
     * error handling
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ErrorEvent event){

        if(!event.getEventKey().equals(this.getClass())) return;

        LogicException e = (LogicException) event.getException();

        //show dialog when error was happened
        DialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(AlertDialogFragment.class.getName())
                .titleRes(e.getErrorCode().getErrorRes())
                .messageRes(e.getReasonRes())
                .positiveRes(android.R.string.ok).build();
        dialogFragment.show(getActivity().getFragmentManager()
                ,AlertDialogFragment.class.getName());

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }


}
