package jp.hellodea.sample.diary.fragment;

import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.sample.common.entity.Diary;
import jp.hellodea.sample.common.eventbus.CallScreenEvent;
import jp.hellodea.sample.common.eventbus.CompleteBackgroundEvent;
import jp.hellodea.sample.common.eventbus.ErrorEvent;
import jp.hellodea.sample.common.eventbus.SelectEvent;
import jp.hellodea.sample.common.exception.LogicException;
import jp.hellodea.sample.common.fragment.AlertDialogFragment;
import jp.hellodea.sample.common.fragment.AlertDialogFragment_;
import jp.hellodea.sample.common.fragment.ProgressDialogFragment;
import jp.hellodea.sample.common.fragment.ProgressDialogFragment_;
import jp.hellodea.sample.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.sample.diary.DiaryModule;
import jp.hellodea.sample.diary.R;
import jp.hellodea.sample.diary.logic.DiaryLogic;
import timber.log.Timber;

/**
 */
@EFragment(resName = "fragment_diary")
@OptionsMenu(resName = "menu_diary")
public class DiaryFragment extends Fragment {

    @Inject
    DiaryLogic diaryLogic;

    @FragmentArg
    String diaryProxyId;


    @ViewById
    ImageView imageViewPicture;

    @ViewById
    TextView textViewTitle;

    @ViewById
    TextView textViewBody;

    @ViewById
    TextView textViewPostTime;

    Diary diary;

    @DebugLog
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Timber.d("diaryProxyId:%s", diaryProxyId);

        //initialize fragment
        setHasOptionsMenu(true);
        ObjectGraphHolder.get().inject(this);

        //get data
        getDiary();

    }

    @DebugLog
    @Override
    public void onStart() {
        super.onStart();

        EventBus.getDefault().registerSticky(this);
    }

    @DebugLog
    @Override
    public void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);
    }

    /**
     * open editor fragment
     */
    @DebugLog
    @OptionsItem
    void actionEdit(){
        EventBus.getDefault().post(new CallScreenEvent(EditFragment.class, diaryProxyId));
    }

    @DebugLog
    @OptionsItem
    void actionDelete(){
        //show dialog when error was happened
        DialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(this.getClass().getName())
                .titleRes(R.string.diary_confirm_title)
                .messageRes(R.string.diary_confirm_delete_diary)
                .positiveRes(android.R.string.ok)
                .negativeRes(android.R.string.cancel).build();
        dialogFragment.show(getActivity().getFragmentManager()
                ,AlertDialogFragment.class.getName());

    }

    /**
     * get diary in background
     */
    @DebugLog
    @Background
    void getDiary(){
        try {
            diary = diaryLogic.getDiary(diaryProxyId);
        } catch (LogicException e) {
            EventBus.getDefault().postSticky(new ErrorEvent(this.getClass(), e));
            return;
        }

        //complete getting diary
        EventBus.getDefault().postSticky(new CompleteBackgroundEvent(this.getClass(),diary));

    }


    /**
     * show diary
     */
    @DebugLog
    public void onEventMainThread(CompleteBackgroundEvent event){
        if(!event.getEventKey().equals(this.getClass())) return;

        Glide.with(getActivity()).load(diary.getImageUrl(false)).into(imageViewPicture);
        textViewTitle.setText(diary.getString(Diary.F_TITLE));
        textViewBody.setText(diary.getString(Diary.F_BODY));

        CharSequence timeString = DateUtils.getRelativeTimeSpanString(
                diary.getDate(Diary.F_POST_TIME).getTime());

        textViewPostTime.setText(timeString);

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }

    /**
     * select dialog
     * @param event
     */
    @DebugLog
    public void onEventMainThread(SelectEvent event){
        if(!event.getEventKey().equals(this.getClass().getName())) return;

        int response = (int) event.getArg();
        if(response == android.R.string.ok) deleteDiary();
    }

    /**
     * error handling
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ErrorEvent event){

        if(!event.getEventKey().equals(this.getClass())) return;

        LogicException e = (LogicException) event.getException();

        //show dialog when error was happened
        DialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(AlertDialogFragment.class.getName())
                .titleRes(e.getErrorCode().getErrorRes())
                .messageRes(e.getReasonRes())
                .positiveRes(android.R.string.ok).build();
        dialogFragment.show(getActivity().getFragmentManager()
                ,AlertDialogFragment.class.getName());

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }

    /**
     * delete this diary
     */
    @DebugLog
    @Background
    void deleteDiary(){

        //show progress dialog while deleting
        ProgressDialogFragment progressDialogFragment = ProgressDialogFragment_.builder()
                .titleRes(R.string.diary_progress_title)
                .messageRes(R.string.diary_progress_deleting)
                .build();
        progressDialogFragment.show(getActivity().getFragmentManager()
                , ProgressDialogFragment.class.getName());

        try {
            diaryLogic.deleteDiary(diary);
        } catch (LogicException e) {
            EventBus.getDefault().postSticky(new ErrorEvent(this.getClass(), e));
            return;
        }

        //close progress dialog after deleting
        progressDialogFragment.dismiss();

        //show diary list screen after saving
        EventBus.getDefault().postSticky(new CallScreenEvent(DiaryListFragment.class));

    }


}
