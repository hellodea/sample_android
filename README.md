# README はじめにお読みください #

This repository is a repository of sample app . Since it has been created in accordance with the standards and conventions , please refer to this for the final product .

このリポジトリはサンプルアプリのリポジトリです。標準化や規約に則って作成されていますので、最終成果物の参考にしてください。

## Overview of sample application (サンプルアプリケーションの概要) ##

This application is a diary application to post photos and text .

このアプリケーションは写真とテキストを投稿する日記アプリケーションです。

![device-2015-02-24-150239.png](https://bitbucket.org/repo/4jzXod/images/3598630224-device-2015-02-24-150239.png)
![device-2015-02-24-150308.png](https://bitbucket.org/repo/4jzXod/images/1429006039-device-2015-02-24-150308.png)
![device-2015-02-24-150401.png](https://bitbucket.org/repo/4jzXod/images/1582761242-device-2015-02-24-150401.png)


### Business requirements (仕様) ###

* [Use Case List](https://docs.google.com/spreadsheets/d/1IxkJktYoAGDeZgKPAfsrZmN-xikG-QBs2-J19OslEw8/edit?usp=sharing)
* [Wireframe](https://docs.google.com/drawings/d/18hoA0g-G4NbwCKNyiBHD7Pxb_NztGxTlQTqjswjz5_M/edit?usp=sharing)
* [Data Model](https://docs.google.com/drawings/d/1wn4HftE0G52TSgpIo5AgV2GAWTe1zokrAHEtWyKkneo/edit?usp=sharing)
* [Screen Function List](https://docs.google.com/spreadsheets/d/1Qa50j679e1WjfvYYCkkw47O9hWLYdg7CpXPz-uEIWtE/edit?usp=sharing)


### Architecture (アーキテクチャ) ###

* [Overall Architecture](https://docs.google.com/drawings/d/1BOwqxXxMqa81wZueWcK1ZX-z1uUTRL4M6vtHlRPuA7o/edit?usp=sharing)
* [Structure in Android](https://docs.google.com/drawings/d/1qtxJPGhvFVCMFId1ut1p-AhzWAhL3QRDRUE8K2rC684/edit?usp=sharing)

### Development Standards (標準・規約) ###

* [Android Development Standards](https://docs.google.com/spreadsheets/d/15yTojoVBhVKlKSkOafbJ2T5Fcb5Jpml1Rd1L7JLifyg/edit?usp=sharing)

